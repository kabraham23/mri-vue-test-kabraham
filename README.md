# MRI Vue developer test

This is a project setup for testing one’s skills as a Vue/TypeScript developer. The starter files were generated using Vue CLI. The project uses the following technologies:

- **Vue 2.** We appreciate that Vue 2 is being phased out for version 3, but we have existing apps built with it. So this test uses Vue 2 to more closely adhere to our real-world apps.
- **TypeScript.** We use TypeScript in our apps and try to adhere to best practices. Well-typed code is much easier to work with, especially when you have a team of developers.
- **Class components.** Because we are using TypeScript, class components work better than the regular Vue syntax. The project includes some starting views to demonstrate the boilerplate for a class component. And here’s the [general guide to class components](https://class-component.vuejs.org/guide/class-component.html).
- **Vue Router.** For navigating single-page applications.
- **ESLint+Prettier.** This helps with code style and formatting. If you’re having trouble with it, feel free to reconfigure it or remove it altogether.

## Project description

This project revolves around a fictional entity called `SpaceDocument`. It’s an object typed as follows:

```ts
interface SpaceDocument {
  access: number[]; // Person.id
  description: string;
  expirationDate: string;
  id: number;
  name: string;
  tags: string[];
}
```

`Person` is another entity:

```ts
interface Person {
  id: number;
  firstName: string;
  lastName: string;
}
```

This project should provide a user interface for displaying a listing of all Documents, as well as a means of editing Documents and creating new ones. Here’s a listing of requirements.

- The home view should display a listing of all Documents along with their metadata.
- Ability to create new Documents.
- Ability to edit existing Documents.
- Name and description are required.
- The create/edit form should allow the user to assign access to a document by assigning `Person` IDs to `SpaceDocument.access`.
- The app doesn’t need to edit or create new `Person` records.

You can do this in the manner you want but one way, for example, would be to create views for creating and editing and navigating between them using the router.

## Database + API

To provide a dummy API to work with, we’ve included [json-server](https://www.npmjs.com/package/json-server) as well as a starting `db.json`.

To start up the API, use:

```sh
npm run db
```

The API will be available at `localhost:3000` and you can get/post/patch to `/documents` and `/people`. Check out the json-server readme for examples. The types mentioned above correspond to the models and data found in `db.json`.

You’re welcome to use `fetch` or bring in an external HTTP client like [axios](https://axios-http.com/).

## Starter files

The project uses Vue CLI establish a starting point. The initial app views are located in `src/views`. We’ve included the starting types in `src/types.ts` and you are welcome to add types there as needed.

We’ve included a `CreateDocumentView` component as an example starting point.

You can run the app with:

```sh
npm run serve
```

## Design

The project doesn’t include any CSS frameworks or component libraries. You’re welcome to include whatever you’re comfortable with.

Our apps typically use [Vuetify](https://vuetifyjs.com/en/) (albeit an older version), but you are in no way required to use that. [Bootstrap](https://getbootstrap.com/), [Bulma](https://bulma.io/), or anything else (or hand-rolled!) is fine. If you’d like a simple suggestion for styles, take a look at [Pico CSS](https://picocss.com/) or the even simpler [Simple CSS](https://simplecss.org/).

## Deliverables

You should create a branch off of `main` and, when you are ready, submit an MR. This exercise is not meant to stump you but rather serve as a jumping off point for conversation. Feel free to timebox this exercise to 2-3 hours and submit it partially completed.
