export interface SpaceDocument {
  access: number[]; // Person.id
  description: string;
  expirationDate: string;
  id: number;
  name: string;
  tags: string[];
}

export interface Person {
  id: number;
  firstName: string;
  lastName: string;
}
